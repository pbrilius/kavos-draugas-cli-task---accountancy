<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\DBAL;

use App\DBAL\EnumType;

/**
 * Description of EnumActionType
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class EnumActionType extends EnumType
{
    const ACTION_BUY = 'buy';
    const ACTION_SELL = 'sell';
    
    const ENUM_ACTIONS = [
        self::ACTION_BUY,
        self::ACTION_SELL,
    ];
    
    protected $name = 'enumaction';
    protected $values = self::ENUM_ACTIONS;
}
