<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Model;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Stockitem;
use App\Repository\StockItemRepository;
use App\DBAL\EnumActionType;

/**
 * Description of StockModel
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class StockModel
{
    /**
     *
     * @var EntityManager
     */
    private $entityManager;
    
    /**
     *
     * @var SymfonyStyle
     */
    private $io;
    
    /**
     *
     * @var array
     */
    private $stockItems;
    
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function getIo(): SymfonyStyle
    {
        return $this->io;
    }

    public function setIo(SymfonyStyle $io)
    {
        $this->io = $io;
        return $this;
    }
    
    public function buyAmount(int $amount, float $price)
    {
        $io = $this->getIo();
        $io->progressStart(1);
        
        $entityManager = $this->getEntityManager();
        
        $stockItem = new Stockitem();
        $stockItem->setPrice($price);
        $stockItem->setQuantity($amount);
        $stockItem->setAction(EnumActionType::ACTION_BUY);
        
        $entityManager->persist($stockItem);
        $entityManager->flush();
        
        $io->progressFinish();
    }
    
    public function sellAmount(int $amount, float $price)
    {
        $io = $this->getIo();
        $io->progressStart(1);
        
        $entityManager = $this->getEntityManager();

        $stockItem = new Stockitem();
        $stockItem->setPrice($price);
        $stockItem->setQuantity($amount);
        $stockItem->setAction(EnumActionType::ACTION_SELL);

        $entityManager->persist($stockItem);
        $entityManager->flush();
        
        $io->progressFinish();
    }
    
    public function totalMargin()
    {
        $io = $this->getIo();
        $entityManager = $this->getEntityManager();
        /* @var $stockItemRepository StockItemRepository */
        $stockItemRepository = $entityManager->getRepository(Stockitem::class);
        $stockItems = $stockItemRepository->findByQuantityAndPriceOrderByCreatedatAsc(
            0,
            -1,
            []
        );
        $this->setStockItems($stockItems);
        $stockItemsSum = count($stockItems);
        $io->progressStart($stockItemsSum);
        $stockItemsCosts = 0;
        $stockItemsTurnover = 0;
        
        /* @var $stockItems Stockitem[] */
        for ($i = 0; $i < $stockItemsSum; $i++) {
            switch ($stockItems[$i]['action']) {
                case EnumActionType::ACTION_BUY:
                    continue;
                case EnumActionType::ACTION_SELL:
                    $stockItemsCosts += $this->deductFifoQuantity($stockItems[$i]['quantity'], $i);
                    $stockItemsTurnover += $stockItems[$i]['quantity'] * $stockItems[$i]['price'];
                    break;
            }
            $io->progressAdvance();
        }
        $io->progressFinish();
        $stockItemsMargin = $stockItemsTurnover - $stockItemsCosts;

        return $stockItemsMargin;
    }
    
    private function deductFifoQuantity(int $quantity, int $cursor)
    {
        /* @var $stockItems Stockitem[] */
        $stockItems = $this->getStockItems();
        $deductedQuantity = 0;
        $stockItemsCosts = 0;
        $i = 0;
        for ($i = 0; $i < $cursor; $i++) {
            if ($stockItems[$i]['action'] == EnumActionType::ACTION_SELL) {
                continue;
            }
            $quantityShiftToDetect = $quantity - $deductedQuantity;
            $maxQuantityToDeduct = $quantityShiftToDetect > $stockItems[$i]['quantity'] ?
                    $stockItems[$i]['quantity']
                    : $quantityShiftToDetect;
            $stockItems[$i]['quantity'] = $stockItems[$i]['quantity'] - $maxQuantityToDeduct;
            $deductedQuantity += $maxQuantityToDeduct;
            $stockItemsCosts += $maxQuantityToDeduct * $stockItems[$i]['price'];
            if ($deductedQuantity == $quantity) {
                break;
            }
        }
        
        $this->setStockItems($stockItems);
        
        return $stockItemsCosts;
    }
    
    public function reset()
    {
        $io = $this->getIo();
        $entityManager = $this->getEntityManager();
        
        /* @var $stockItemRepository StockItemRepository */
        $stockItemRepository = $entityManager->getRepository(Stockitem::class);
        $stockItems = $stockItemRepository->findAll();
        
        $io->progressStart(count($stockItems));
        
        foreach ($stockItems as $stockItem) {
            $entityManager->remove($stockItem);
            $entityManager->flush();
            
            $io->progressAdvance();
        }
        
        $io->progressFinish();
    }
    
    public function getStockItems()
    {
        return $this->stockItems;
    }

    public function setStockItems($stockItems)
    {
        $this->stockItems = $stockItems;
        return $this;
    }
}
