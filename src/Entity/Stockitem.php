<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\DBAL\EnumActionType;

/**
 * Stockitem
 *
 * @ORM\Table(name="stockItem")
 * @ORM\Entity(repositoryClass="App\Repository\StockItemRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Stockitem
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Column(name="id", type="uuid", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;
    
    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=16, scale=2, nullable=false)
     */
    private $price;
    
    /**
     * @var \App\DBAL\EnumActionType
     *
     * @ORM\Column(type="enumaction")
     */
    private $action;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdat", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updatedat", type="datetime", nullable=true)
     */
    private $updatedat;
    

    public function getId()
    {
        return $this->id;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getCreatedat(): ?\DateTimeInterface
    {
        return $this->createdat;
    }

    public function setCreatedat(\DateTimeInterface $createdat): self
    {
        $this->createdat = $createdat;

        return $this;
    }

    public function getUpdatedat(): ?\DateTimeInterface
    {
        return $this->updatedat;
    }

    public function setUpdatedat(?\DateTimeInterface $updatedat): self
    {
        $this->updatedat = $updatedat;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
    
    public function getAction(): EnumActionType
    {
        return $this->action;
    }

    public function setAction(string $action)
    {
        $this->action = $action;
        return $this;
    }
    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdat = new \DateTime();
    }
    
    /**
     * ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedat = new DateTime();
    }
}
