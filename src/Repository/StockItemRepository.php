<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Repository;

use App\Repository\QueryBuilder\StockItem;
use Doctrine\Common\Collections\ExpressionBuilder;
use Doctrine\ORM\Query\Expr;
use Doctrine\Common\Collections\Criteria;

/**
 * Stock Item entity repository
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class StockItemRepository extends StockItem
{
    public function __construct($entityManager)
    {
        parent::__construct($entityManager);

        $qb = $this->getQb();

        $qb
                ->addOrderBy(new Expr\OrderBy('s.createdat', 'ASC'));

        $this->setQb($qb);
    }

    
    public function findByQuantityAndPriceOrderByCreatedatAsc(
        int $offset,
        int $limit,
        array $parameters
    ) {
        $qb = $this->getQb();
        $qb->setFirstResult($offset);
        if ($limit != -1) {
            $qb->setMaxResults($limit);
        }
        
        foreach ($parameters as $key => $parameter) {
            /* @var $criteriaExpression ExpressionBuilder */
            $criteriaExpression = Criteria::expr();
            $criteria           = Criteria::create()
                   ->where($criteriaExpression->eq(
                       's.' . $key,
                       $parameter
                   ));
            $qb->addCriteria($criteria);
        }
        
        return $qb
                ->getQuery()
                ->getResult();
    }
}
