<?php

namespace App\Repository\QueryBuilder;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManager;
use App\Entity\Stockitem as StockitemEntity;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * Description of StockItem
 *
 * @author paul
 */
abstract class StockItem extends EntityRepository
{
    /**
     *
     * @var QueryBuilder
     */
    protected $qb;

    public function __construct(EntityManager $entityManager)
    {
        $metadata = new ClassMetadata(StockitemEntity::class);
        parent::__construct($entityManager, $metadata);

        $this->qb = $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('s.id')
            ->addSelect('s.quantity')
            ->addSelect('s.price')
            ->addSelect('s.action')
            ->addSelect('s.createdat')
            ->addSelect('s.updatedat')
            ->from(StockitemEntity::class, 's');
    }

    public function getQb()
    {
        return $this->qb;
    }

    public function setQb($qb)
    {
        $this->qb = $qb;
        return $this;
    }
}
