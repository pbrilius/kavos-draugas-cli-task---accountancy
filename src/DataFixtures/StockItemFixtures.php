<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Stockitem;
use App\DBAL\EnumActionType;

class StockItemFixtures extends Fixture
{
    private static $compoundAmount = 256;
    
    public function load(ObjectManager $manager)
    {
        $actionArray = EnumActionType::ENUM_ACTIONS;
        for ($i = 0; $i < self::$compoundAmount; $i++) {
            $stockItem = new Stockitem();
            $stockItem->setQuantity(mt_rand(100, 2000));
            $stockItem->setPrice(mt_rand(10, 100));
            $stockItem->setAction($actionArray[array_rand($actionArray)]);

            $manager->persist($stockItem);
            $manager->flush();
        }
    }
}
