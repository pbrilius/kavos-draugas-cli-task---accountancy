<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Model\StockModel;

/**
 * Resets stock items, passing operational database behaviour to
 * the Stock Item model
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class ResetCommand extends Command
{
    /**
     *
     * @var StockModel
     */
    private $stockModel;
    
    public function __construct(StockModel $stockModel)
    {
        parent::__construct();
        $this->stockModel = $stockModel;
    }
    
    protected function configure()
    {
        $this
                ->setName('app:reset')
                ->setDescription('Reset stored database buying/selling trade queue')
                ->setHelp('This command allows you to buy'
                        . ' stock items in order to generate'
                        . ' Total Margin by FIFO');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Reset Total Margin Generator');
        $io->section('Resetting');
        
        $stockModel = $this->getStockModel();
        
        try {
            $io->createProgressbar();
            $stockModel->setIo($io);
            $stockModel->reset();
            $io->success('Generator-counter reset');
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
    }
    
    public function getStockModel(): StockModel
    {
        return $this->stockModel;
    }

    public function setStockModel(StockModel $stockModel)
    {
        $this->stockModel = $stockModel;
        return $this;
    }
}
