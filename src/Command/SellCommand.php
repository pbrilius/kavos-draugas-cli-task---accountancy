<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Model\StockModel;

/**
 * Sells stock items, passing operational database behaviour to
 * the Stock Item model
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class SellCommand extends Command
{
    
    /**
     *
     * @var StockModel
     */
    private $stockModel;
    
    public function __construct(StockModel $stockModel)
    {
        parent::__construct();
        $this->stockModel = $stockModel;
    }

    protected function configure()
    {
        $this
                ->setName('app:sell')
                ->setDescription('Sell some coffee stock')
                ->setHelp('This command allows you to sell'
                        . ' stock items in order to generate'
                        . ' Total Margin by FIFO')
                ->addArgument(
                    'quantity',
                    InputArgument::REQUIRED,
                    'Selling quantity'
                )
                ->addArgument(
                    'price',
                    InputArgument::REQUIRED,
                    'Selling price'
                )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Sell');
        $io->section('Selling');

        $stockModel = $this->getStockModel();

        $sellQuantity = $input->getArgument('quantity');
        $sellPrice = $input->getArgument('price');

        try {
            $io->createProgressbar();
            $stockModel->setIo($io);
            $stockModel->sellAmount($sellQuantity, $sellPrice);
            $io->success('Sold '
                    . $sellQuantity
                    . ' items for '
                    . $sellPrice
                    . ' currency units');
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
    }
    
    public function getStockModel(): StockModel
    {
        return $this->stockModel;
    }

    public function setStockModel(StockModel $stockModel)
    {
        $this->stockModel = $stockModel;
        return $this;
    }
}
