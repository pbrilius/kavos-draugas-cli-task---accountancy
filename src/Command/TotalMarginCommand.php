<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Model\StockModel;

/**
 * Generates/deducts stock items total margin sum, passing operational
 * database behaviour to the Stock Item model
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class TotalMarginCommand extends Command
{
    
    /**
     *
     * @var StockModel
     */
    private $stockModel;

    public function __construct(StockModel $stockModel)
    {
        parent::__construct();
        $this->stockModel = $stockModel;
    }

    protected function configure()
    {
        $this
                ->setName('app:total-margin')
                ->setDescription('Generate total margin')
                ->setHelp('This command allows you to generate'
                        . ' total margin sum by FIFO based on bought'
                        . ' and sold stock quantities');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Total margin');
        $io->section('Deducting Sum');

        $stockModel = $this->getStockModel();

        try {
            $io->createProgressbar();
            $stockModel->setIo($io);
            $totalMargin = $stockModel->totalMargin($io);
            $io->success('Total margin sum deducted');
            $io->section('Total (Sum) (Currency Units)');
            $io->block($totalMargin);
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
    }
    
    public function getStockModel(): StockModel
    {
        return $this->stockModel;
    }

    public function setStockModel(StockModel $stockModel)
    {
        $this->stockModel = $stockModel;
        return $this;
    }
}
