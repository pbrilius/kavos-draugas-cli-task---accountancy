<?php

/*
 * Copyright (c) <pixelpitcteam.tumblr.com> (tm) 2017 - 2018
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Model\StockModel;

/**
 * Buys stock items, passing operational database behaviour to
 * the Stock Item model
 *
 * @author Povilas Brilius <pbrilius@gmail.com>
 */
class BuyCommand extends Command
{
    /**
     *
     * @var StockModel
     */
    private $stockModel;
    
    public function __construct(StockModel $stockModel)
    {
        parent::__construct();
        $this->stockModel = $stockModel;
    }
    
    protected function configure()
    {
        $this
                ->setName('app:buy')
                ->setDescription('Buy some coffee stock')
                ->setHelp('This command allows you to buy'
                        . ' stock items in order to generate'
                        . ' Total Margin by FIFO')
                ->addArgument(
                    'quantity',
                    InputArgument::REQUIRED,
                    'Buying quantity'
                )
                ->addArgument(
                    'price',
                    InputArgument::REQUIRED,
                    'Buying price'
                )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Buy');
        $io->section('Buying');
        
        $stockModel = $this->getStockModel();
        
        $buyQuantity = $input->getArgument('quantity');
        $buyPrice = $input->getArgument('price');
        
        try {
            $io->createProgressbar();
            $stockModel->setIo($io);
            $stockModel->buyAmount($buyQuantity, $buyPrice);
            $io->success('Bought '
                    . $buyQuantity
                    . ' items for '
                    . $buyPrice
                    . ' currency units');
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
    }
    
    public function getStockModel(): StockModel
    {
        return $this->stockModel;
    }

    public function setStockModel(StockModel $stockModel)
    {
        $this->stockModel = $stockModel;
    }
}
