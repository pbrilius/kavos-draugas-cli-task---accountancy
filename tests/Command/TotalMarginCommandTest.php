<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class TotalMarginCommandTest extends KernelTestCase
{
    public function testSomething()
    {
        $kernel = self::bootKernel();
        $kernel->boot();

        $application = new Application($kernel);

        $command       = $application->find('app:total-margin');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command'  => $command->getName(),
        ));

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertContains('Total margin sum deducted', $output);
        $this->assertContains('Total (Sum)', $output);
    }
}
