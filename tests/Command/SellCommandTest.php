<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class SellCommandTest extends KernelTestCase
{
    public function testSomething()
    {
        $kernel = self::bootKernel();
        $kernel->boot();

        $application = new Application($kernel);

        $quantity = 8;
        $price    = 36;

        $command       = $application->find('app:sell');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command'  => $command->getName(),
            'quantity' => $quantity,
            'price'    => $price,
        ));

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertContains(
            'Sold '
                . $quantity
                . ' items for '
                . $price,
            $output
        );
    }
}
