<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class BuyCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel      = self::bootKernel();
        $kernel->boot();
        
        $application = new Application($kernel);
        
        $quantity = 16;
        $price = 24;

        $command       = $application->find('app:buy');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command' => $command->getName(),
            'quantity' => $quantity,
            'price' => $price,
        ));

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertContains(
            'Bought '
                . $quantity
                . ' items for '
                . $price,
            $output
        );
    }
}
