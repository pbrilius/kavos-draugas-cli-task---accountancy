# Installation And Setup

Zipped file already has all the vendor prerequisites, so you can run the app
out of the box - as requested by you (Task description).

## Database setup

Database parameters are stored in (.env), arrange database (create user
with password, grant access to the database) yourself manually and test access
for that user connection.

```
cp .env .env.local
source .env.local
```

* Database schema setup

Provided that database is created and user has access granted, execute
```
bin/console doctrine:schema:create
bin/console doctrine:schema:validate
```

If the database isn't validated (usually stage no. 2, because of Doctrine
limitations), then reiterate
```
bin/console doctrine:schema:update --dump-sql --force
```
as many times as needed.

## Commands

It's using Symfony Console, as described, list commands by 
```
bin/console list
```
The section **app** contains the task commands, namely processing stock items
in the market. Command help is displayed by
```
bin/console app:buy --help
```
Pass command arguments and process orders, such as in the task description:
```
bin/console app:buy 10 17
bin/console app:sell 6 21
bin/console app:buy 10 20
bin/console app:sell 8 23
```
Generate Total Margin Sum:
```
bin/console app:total-margin
```
See it's the correct 60 currency units, as in the task file.

Reset database accumulated data:
```
bin/console app:reset
``` 

Unit cases:
```
cp phpunit.xml.dist phpunit.xml
phpunit
```

Unit cases with coverage:
```
phpunit --cov-html=cov
```

## N. B.

Execute commands one by one, not to mess up the console :)

Also have in mind that app has been tested with additional selling/buying
trade queues, not too sophisticated, ant mathematically the margin generated
sum was correct.

In case you fail to setup database, a backup dump is included in (backup/)
folder.