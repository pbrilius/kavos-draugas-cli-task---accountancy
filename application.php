#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use App\Command\BuyCommand;
use App\Command\SellCommand;
use App\Command\TotalMarginCommand;

$application = new Application();

$application->add($application->find(BuyCommand::class));
$application->add($application->find(SellCommand::class));
$application->add($application->find(TotalMarginCommand::class));

$application->run();
